from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired
from flask import Flask
from flask import render_template, redirect, session, request
import datetime

import sqlite3


class DB:
    def __init__(self):
        conn = sqlite3.connect('news.db', check_same_thread=False)
        self.conn = conn

    def get_connection(self):
        return self.conn

    def __del__(self):
        self.conn.close()


class UsersModel:
    def __init__(self, connection):
        self.connection = connection

    def init_table(self):
        cursor = self.connection.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS users 
                            (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                             user_name VARCHAR(50),
                             password_hash VARCHAR(128)
                             )''')
        cursor.close()
        self.connection.commit()

    def insert(self, user_name, password_hash):
        cursor = self.connection.cursor()
        cursor.execute('''INSERT INTO users 
                          (user_name, password_hash) 
                          VALUES (?,?)''', (user_name, password_hash))
        cursor.close()
        self.connection.commit()

    def get(self, user_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM users WHERE id = ?", (str(user_id),))
        row = cursor.fetchone()
        return row

    def get_all(self):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM users")
        rows = cursor.fetchall()
        return rows

    def exists(self, user_name, password_hash):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM users WHERE user_name = ? AND password_hash = ?",
                       (user_name, password_hash))
        row = cursor.fetchone()
        return (True, row[0]) if row else (False,)

class NewsModel:
    def __init__(self, connection):
        self.connection = connection

    def init_table(self):
        cursor = self.connection.cursor()
        cursor.execute('''CREATE TABLE IF NOT EXISTS news2 
                            (id INTEGER PRIMARY KEY AUTOINCREMENT, 
                             title VARCHAR(100),
                             content VARCHAR(1000),
                             data VARCHAR(100),
                             user_id INTEGER
                             )''')
        cursor.close()
        self.connection.commit()

    def insert(self, title, content, data, user_id):
        cursor = self.connection.cursor()
        cursor.execute('''INSERT INTO news2 
                          (title, content, data, user_id) 
                          VALUES (?,?,?,?)''', (title, content, data, str(user_id)))
        cursor.close()
        self.connection.commit()

    def get(self, news_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM news2 WHERE id = ?", (str(news_id),))
        row = cursor.fetchone()
        return row

    def get_user(self, user_id):
        cursor = self.connection.cursor()
        cursor.execute("SELECT * FROM news2 WHERE user_id = ?", (str(user_id),))
        row = cursor.fetchone()
        return row

    def get_all(self, user_id=None):
        cursor = self.connection.cursor()
        if user_id:
            cursor.execute("SELECT * FROM news2 WHERE user_id = ?",
                           (str(user_id),))
        else:
            cursor.execute("SELECT * FROM news2")
        rows = cursor.fetchall()
        return rows

    def delete(self, news_id):
        cursor = self.connection.cursor()
        cursor.execute('''DELETE FROM news2 WHERE id = ?''', (str(news_id),))
        cursor.close()
        self.connection.commit()

app = Flask(__name__)
app.config['SECRET_KEY'] = 'yandexlyceum_secret_key'
db = DB()


class LoginForm(FlaskForm):
    username = StringField('Логин', validators=[DataRequired()])
    password = PasswordField('Пароль', validators=[DataRequired()])
    remember_me = BooleanField('Запомнить меня')
    submit = SubmitField('Войти')

class AddNewsForm(FlaskForm):
    title = StringField('Заголовок новости', validators=[DataRequired()])
    content = TextAreaField('Текст новости', validators=[DataRequired()])
    submit = SubmitField('Добавить')

def init_tables():
    global nm, user_model
    user_model = UsersModel(db.get_connection())
    user_model.init_table()
    nm = NewsModel(db.get_connection())
    nm.init_table()

init_tables()

@app.route('/')
@app.route('/index')
def index():
    user = "Ученик Яндекс.Лицея"
    if 'user_id' in session:
        if session['is_admin']:
            li = sort_news(nm.get_all())
        else:
            li = sort_news(nm.get_all(session['user_id']))
    else:
        li = [('', 'Зайдите в аккаунт', '(логин: Admin, пароль: admin)', '')]
    print(li)
    return render_template('index.html', title='Домашняя страница',
                           username=user, content_list=li, user_model = user_model)

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == 'POST':
        user_name = form.username.data
        password = form.password.data
        exists = user_model.exists(user_name, password)
        print(user_model.get_all())
        if (exists[0]):
            session['username'] = user_name
            if user_name == 'Admin' and password == 'admin':
                session['is_admin'] = True
            else:
                session['is_admin'] = False
            session['user_id'] = exists[1]
        return redirect("/index")
    return render_template('login.html', form = form)

@app.route('/registration', methods=['GET', 'POST'])
def registration():
    form = LoginForm()
    if request.method == 'POST':
        user_name = form.username.data
        password = form.password.data
        user_model.insert(user_name, password)
        exists = user_model.exists(user_name, password)
        print(user_model.get_all())
        if (exists[0]):
            session['username'] = user_name
            session['user_id'] = exists[1]
            if user_name == 'Admin' and password == 'admin':
                session['is_admin'] = True
            else:
                session['is_admin'] = False
        return redirect("/index")
    return render_template('registration.html', form = form)


@app.route('/logout')
def logout():
    session.pop('username',0)
    session.pop('user_id', 0)
    return redirect('/login')


@app.route('/add_news', methods=['GET', 'POST'])
def add_news():
    if 'username' not in session:
        return redirect('/login')
    form = AddNewsForm()
    if form.validate_on_submit():
        title = form.title.data
        content = form.content.data
        now = datetime.datetime.now()                     #
        nm.insert(title, content, '/'.join(list(map(lambda x: str(x), [now.day, now.month, now.year]))), session['user_id'])
        print(nm.get_all())
        return redirect("/index")
    return render_template('add_news.html', title='Добавление новости',
                           form=form, username=session['username'])


@app.route('/delete_news/<int:news_id>', methods=['GET'])
def delete_news(news_id):
    if 'username' not in session:
        return redirect('/login')
    nm = NewsModel(db.get_connection())
    nm.delete(news_id)
    return redirect("/index")

def get_last_user_id():
    return user_model.get_all()[-1][0]

def sort_news(news_list):
    news_list = sorted(news_list, key=lambda x: (int(x[3].split('/')[2]),int(x[3].split('/')[1]),int(x[3].split('/')[0]), x[1]))
    return news_list

if __name__ == '__main__':
    app.run(port=8043, host='127.0.0.1')
